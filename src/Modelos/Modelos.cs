namespace Modelos{
    
    public enum Sexo{
        H,
        M
    }

    public enum Especie{
        Perro,
        Gato,
        Cerdo,
        Hamster,
        Iguana,
        Loro,
        Poni
    }

    public class Socio {
        public string nombre{get; set;}
        public Sexo sexo{get;set;}
        public string dni{get; set;}

        public override string ToString()
        {
            return $"Nombre: {nombre}, Sexo: {sexo}, DNI: {dni}";
        }
    }

    public class Mascota {
        public string nombre{get;set;}
        public Especie especie{get;set;}
        public int Edad{get;set;}
        public string IDSocio{get;set;}
        public DateTime fecha{get;set;}
        public override string ToString()
        {
            return $"{nombre}, {especie}, {Edad} años ({dueño.nombre})";
        }
        public Socio dueño{get;set;}
    }
    
}