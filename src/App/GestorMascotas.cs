﻿using data;
using Modelos;
namespace App{

    public class GestorMascotas
    {   
        public List<Mascota> mascotas{get;}
        public List<Socio> socios{get;}
        private SociosCSV repoSocios;
        private MascotasCSV repoMascotas;

        public GestorMascotas(SociosCSV repoSocios, MascotasCSV repoMascotas){
            this.repoMascotas=repoMascotas;
            this.repoSocios=repoSocios;

            mascotas = repoMascotas.Leer();
            socios = repoSocios.Leer();

            foreach(var mascota in mascotas)
            {
                Socio resul = buscarSocio(mascota.IDSocio);
                mascota.dueño=resul;
            }
        }

        public Socio buscarSocio(string dni)
        {
            return socios.Find(socio => socio.dni.Equals(dni));
        } 

        public void altaSocio(Socio socio){
            socios.Add(socio);
            repoSocios.Guardar(socios);
        }

        public void bajaSocio(Socio socio){
            socios.Remove(socio);
            repoSocios.Guardar(socios);
        }
        public void altaMascota(Mascota mascota){
            mascotas.Add(mascota);
            repoMascotas.Guardar(mascotas);
        }
        public void bajaMascota(Mascota mascota){
            mascotas.Remove(mascota);
            repoMascotas.Guardar(mascotas);
        }

        public List<Mascota> mascotasDeSocio(Socio socio) 
        {
            return mascotas.FindAll(mascota => socio.dni.Equals(mascota.IDSocio));
        }

        public List<Mascota> mascotasList()
        {    
            return mascotas;
        }

        public void cambiarMascota(Mascota mascota, Socio socio){
            mascota.IDSocio = socio.dni;
            mascota.dueño = socio;
            repoMascotas.Guardar(mascotas);
        }
    }
}