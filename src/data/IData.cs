using System;
using System.Collections.Generic;

namespace data{
    public interface IData<T>{
        public List<T> Leer();
        public void Guardar(List<T> lista);
    }
}