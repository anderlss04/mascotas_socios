﻿using Modelos;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace data{
    public class MascotasCSV : IData<Mascota>
    {   
        string archivo="../../DatosMascotas.csv";

        public void Guardar(List<Mascota> mascotas){
            List<string> datos = new(){};
            
            foreach(var mascota in mascotas)
            {
                string valores = $"{mascota.nombre},{mascota.especie},{mascota.fecha.ToString("ddMMyyyy")},{mascota.IDSocio}";

                datos.Add(valores);
            }

            File.WriteAllLines(archivo,datos);
        }


        public List<Mascota> Leer(){
            List<Mascota> mascotas = new();
            var datos = File.ReadAllLines(archivo).ToList();

            foreach(var fila in datos)
            {
                var valores = fila.Split(",");
                var mascota = new Mascota{
                    nombre=valores[0], 
                    especie = (Especie)Enum.Parse(typeof(Especie),valores[1]),
                    fecha = DateTime.ParseExact(valores[2],"ddMMyyyy",CultureInfo.InvariantCulture),
                    IDSocio=valores[3]};

                mascotas.Add(mascota);
            }
            return mascotas;
        }

    }
    public class SociosCSV : IData<Socio>{
        string archivo = "../../DatosSocios.csv"; 
        public List<Socio> Leer(){
            List<Socio> socios = new();
            var datos = File.ReadAllLines(archivo).ToList();

            foreach (var fila in datos)
            {
                var valores = fila.Split(",");

                var socio = new Socio{
                    nombre = valores[0],
                    sexo = (Sexo) Enum.Parse(typeof(Sexo),valores[1]),
                    dni = valores[2],
                    };
                
                socios.Add(socio);
            }
            return socios;
        }

        public void Guardar(List<Socio> socios){
            List<string> datos = new(){};

            foreach(var socio in socios)
            {
                string valores = $"{socio.nombre},{socio.sexo},{socio.dni}";
                datos.Add(valores);
            }

            File.WriteAllLines(archivo,datos);
        }
    }


}
