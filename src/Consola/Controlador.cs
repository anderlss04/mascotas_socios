using App;
using Modelos;

namespace Consola
{
    public class Controlador
    {

        public GestorMascotas gestor;

        public Vista vista = new Vista();
        public Dictionary<string, Action> casosDeUso;
        public Controlador(GestorMascotas gestor)
        {
            this.gestor = gestor;

            casosDeUso = new Dictionary<string, Action>(){
            {"Añadir nuevo socio",altaSocio},
            {"Eliminar socio",bajaSocio},
            {"Añadir nueva mascota",altaMascota},
            {"Eliminar mascota",bajaMascota},
            {"Comprar una mascota",cambiarDueño},
            {"Listar mascotas",ListaMascotas},
            {"Buscar la mascotas de un socio",buscarMascotasSocio},
            {"Listar socios",verSocios}
        };
        }

        public void Run()
        {
            while (true)
            {
                try
                {
                    string eleccion = vista.TryObtenerElementoDeLista("MENU", casosDeUso.Keys.ToList(), "Elija una opcion");
                    casosDeUso[eleccion].Invoke();
                    vista.MostrarYReturn("Pulsa <Return> para continuar");
                    vista.LimpiarPantalla();
                }
                catch { return; }
            }
        }

        public void altaSocio()
        {
            string _dni = vista.TryObtenerDatoDeTipo<string>("Introduzca su DNI");
            if (gestor.buscarSocio(_dni) == null)
            {
                string name = vista.TryObtenerDatoDeTipo<string>("Introduzca su nombre");
                Sexo sex = vista.TryObtenerElementoDeLista<Sexo>("Introduzca su sexo", vista.EnumToList<Sexo>(), "Introduzca numero de la lista");
                Socio socio = new Socio
                {
                    dni = _dni,
                    nombre = name,
                    sexo = sex
                };
                gestor.altaSocio(socio);
            }
            else { vista.Mostrar("ERROR!!!! Existe otro socio con el mismo DNI"); }
        }

        public void bajaMascota()
        {
            List<Mascota> mascotas = gestor.mascotas;
            Mascota mascota = vista.TryObtenerElementoDeLista("Introduzca la mascota", mascotas, "Introduzca numero de la lista");
            if (vista.Confirmar("Seguro que quiere dar de baja a la mascota?"))
            {
                gestor.bajaMascota(mascota);
            }
        }

        public void altaMascota()
        {
            List<Socio> socios = gestor.socios;
            Socio socio = vista.TryObtenerElementoDeLista("Seleccione el socio dueño de la mascota", socios, "Introduzca numero de la lista");
            string nombreMascota = vista.TryObtenerDatoDeTipo<string>("Introduzca el nombre de la mascota");
            Especie especieMasc = vista.TryObtenerElementoDeLista<Especie>("Que especie es la mascota?", vista.EnumToList<Especie>(), "Introduzca numero de la lista");
            DateTime fnnMascot = vista.TryObtenerFecha("Introduzca la fecha de nacimiento de la mascota");
            Mascota mascota = new Mascota
            {
                fecha = fnnMascot,
                nombre = nombreMascota,
                especie = especieMasc,
                IDSocio = socio.dni,
                dueño = socio,
            };
            gestor.altaMascota(mascota);
        }

        public void buscarMascotasSocio()
        {
            List<Socio> socios = gestor.socios;
            Socio socio = vista.TryObtenerElementoDeLista("De que socio desea buscar la mascota?", socios, "Introduzca numero de la lista");
            List<Mascota> mascotas = gestor.mascotasDeSocio(socio);
            vista.MostrarListaEnumerada($"Las mascotas de {socio.nombre}", mascotas);
        }

        public void verSocios()
        {
            List<Socio> socios = gestor.socios;
            vista.MostrarListaEnumerada("Lista de socios", socios);
        }

        public void cambiarDueño()
        {
            List<Mascota> mascotas = gestor.mascotas;
            Mascota mascota = vista.TryObtenerElementoDeLista("Seleccione una mascota", mascotas, "Introduzca numero de la lista");
            List<Socio> socios = gestor.socios;
            Socio socio = vista.TryObtenerElementoDeLista("Seleccione el nuevo dueño de la mascota", socios, "Introduzca numero de la lista");
            gestor.cambiarMascota(mascota, socio);
        }
        public void ListaMascotas()
        {
            List<Mascota> mascotas = gestor.mascotasList();
            vista.MostrarListaEnumerada("Lista de mascotas", mascotas);
        }
        public void bajaSocio()
        {
            List<Socio> socios = gestor.socios;
            Socio socio = vista.TryObtenerElementoDeLista("Seleccione el socio que quiere dar de baja", socios, "Introduzca numero de la lista");
            List<Mascota> mascotas = gestor.mascotasDeSocio(socio);
            vista.MostrarListaEnumerada("Estas mascotas serán eliminadas", mascotas);
            if (vista.Confirmar("Seguro que quiere dar de baja al socio con sus mascotas?"))
            {
                mascotas.ForEach(mascota => gestor.bajaMascota(mascota));
                gestor.bajaSocio(socio);
            }
        }
       
    }
}