﻿using data;
using App;
using Consola;

var repoMascotas = new MascotasCSV();
var repoSocios = new SociosCSV();
var gestor = new GestorMascotas(repoSocios,repoMascotas);
var controlador = new Controlador(gestor);
controlador.Run();

